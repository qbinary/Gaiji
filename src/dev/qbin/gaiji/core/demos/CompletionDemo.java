package dev.qbin.gaiji.core.demos;

import java.io.IOException;
import java.nio.file.Paths;

import dev.qbin.gaiji.exception.GaijiConfigException;
import dev.qbin.gaiji.exception.GaijiHttpException;
import dev.qbin.gaiji.openai.api.OpenAiAPI;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.Completion;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CompletionRequestBuilder.CompletionRequest;
import dev.qbin.gaiji.util.GaijiConfig;

public class CompletionDemo {
	public static void main(String[] args) throws GaijiConfigException, IOException, GaijiHttpException {
		GaijiConfig config = new GaijiConfig(Paths.get("gaiji.conf"));

		// Initialize API
		OpenAiAPI api = new OpenAiAPI(config.getApiToken());

		// Build request
		CompletionRequest request = new OpenAiRequest.CompletionRequestBuilder().setPrompt("Hello how are you?\n")
				.setModel("text-davinci-002").build();

		// Execute request
		Completion completion = api.createCompletion(request);

		System.out.println(completion.choices[0].text);
	}
}
