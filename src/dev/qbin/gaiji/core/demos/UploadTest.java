package dev.qbin.gaiji.core.demos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.GsonBuilder;

import dev.qbin.gaiji.exception.GaijiConfigException;
import dev.qbin.gaiji.openai.api.OpenAiAPI;
import dev.qbin.gaiji.util.GaijiConfig;

public class UploadTest {
	public static void main(String[] args) throws GaijiConfigException, IOException {
		GaijiConfig config = new GaijiConfig(Paths.get("gaiji.conf"));
		OpenAiAPI api = new OpenAiAPI(config.getApiToken());
		System.out.println(new GsonBuilder().setPrettyPrinting().create()
				.toJson(api.uploadFile(Files.readAllBytes(Paths.get("test.jsonl")), "testFile")));
	}
}
