package dev.qbin.gaiji.core.demos;

import java.io.IOException;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dev.qbin.gaiji.GaijiService;
import dev.qbin.gaiji.exception.GaijiConfigException;
import dev.qbin.gaiji.exception.GaijiHttpException;
import dev.qbin.gaiji.exception.GaijiImpossibleException;
import dev.qbin.gaiji.openai.api.OpenAiAPI;
import dev.qbin.gaiji.util.GaijiConfig;

public class EngineEditorTest {
	public static void main(String[] args)
			throws GaijiConfigException, IOException, GaijiHttpException, GaijiImpossibleException {
		// Config
		GaijiConfig config = new GaijiConfig(Paths.get("./gaiji.conf"));

		GaijiService.getInstance().setApiToken(config.getApiToken());

		// Api
		OpenAiAPI api = new OpenAiAPI(config.getApiToken());

		// Json
		Gson json = new GsonBuilder().setPrettyPrinting().create();
		
		// Demo
//		System.out.println(json.toJson(api.getEngines()));
		System.out.println(json.toJson(api.getFineTunes()));

//		GPT3 gpt3 = new GPT3();
//		System.out.println(gpt3.request(new GPT3CompletionRequest.Builder()
//				.setPrompt("Hello, where is Abraxas, the leader of the personalities?").setModel("text-davinci-002")
//				.setTemperature(0.7).setMax_tokens(100).request()).choices[0].text);
	}
}
