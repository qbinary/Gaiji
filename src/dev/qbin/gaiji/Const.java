package dev.qbin.gaiji;

public class Const {
	public static final String PROJECT_SIMPLE_NAME = "gaiji";
	public static final String PROJECT_CHARSET = "UTF-8";

	public static final class Mime {
		public static final String JSON = "application/json";
		public static final String OCTET_STREAM = "application/octet-stream";
	}

	public static final class Http {
		public static final String POST = "POST";
		public static final String DELETE = "DELETE";
		public static final String GET = "GET";

		public static final class Header {
			public static final String AUTHORIZATION = "Authorization";
			public static final String CONTENT_TYPE = "Content-Type";
		}
	}

	public static final class API {
		public static final String AUTHORIZATION_TEMPLATE = "Bearer %s";

		public static final String STOCK_COMPLETION_TEMPLATE = "https://api.openai.com/v1/engines/%s/completions";
		public static final String TUNE_COMPLETIONS = "https://api.openai.com/v1/completions";
		public static final String ENGINES = "https://api.openai.com/v1/engines";
		public static final String CANCEL_FINE_TUNE = "https://api.openai.com/v1/fine-tunes/%s/cancel";
		public static final String DELETE_MODEL_TEMPLATE = "https://api.openai.com/v1/models/%s";

		public static final String FINE_TUNES = "https://api.openai.com/v1/fine-tunes";
		public static final String FILES = "https://api.openai.com/v1/files";

		public static final String FINE_TUNES_TEMPLATE = "https://api.openai.com/v1/fine-tunes/%s";
	}

	public static final class Config {
		public static final String API_TOKEN = "API_TOKEN";

		public static final String[] REQUIRED_KEYS = { API_TOKEN };
	}

	public static final class CLI {
		public static final String FINE_TUNE_CREATE_TEMPLATE = "openai api fine_tunes.create -t %s -m %s --suffix '%s'";
	}

	public static final class Text {
		public static final String CONFIG_KEY_MISSING = "The config key '%s' is mendatory!";
		public static final String API_TOKEN_NOT_SET = "Implicit token retrieval failed. Token not set in GaijiService.";
	}
}
