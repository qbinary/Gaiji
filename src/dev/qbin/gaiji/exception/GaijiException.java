package dev.qbin.gaiji.exception;

public abstract class GaijiException extends Exception {
	private static final long serialVersionUID = 658244349629257324L;
	
	public GaijiException(final String message) {
		super(message);
	}
}
