package dev.qbin.gaiji.exception;

public class GaijiImpossibleException extends RuntimeException {

	private static final long serialVersionUID = 5562892145477396556L;

	public GaijiImpossibleException(final String string) {
		super(string);
	}
}
