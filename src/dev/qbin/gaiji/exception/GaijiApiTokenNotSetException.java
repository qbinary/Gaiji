package dev.qbin.gaiji.exception;

import dev.qbin.gaiji.Const;

public class GaijiApiTokenNotSetException extends GaijiImpossibleException {
	private static final long serialVersionUID = -4687841149080739053L;

	public GaijiApiTokenNotSetException() {
		super(Const.Text.API_TOKEN_NOT_SET);
	}
}
