package dev.qbin.gaiji.exception;

public class GaijiTuneException extends GaijiException {
	private static final long serialVersionUID = 7289202258962814458L;

	public GaijiTuneException(String message) {
		super(message);
	}
}
