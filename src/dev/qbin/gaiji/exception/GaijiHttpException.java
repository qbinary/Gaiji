package dev.qbin.gaiji.exception;

public class GaijiHttpException extends GaijiException {
	private static final long serialVersionUID = 7415238248217013015L;

	public enum ErrorType {
		OPEN_URL_CONNECTION, REQUEST_METHOD, WRITE_BODY, IMPOSSIBLE, RESPONSE_RETRIEVAL
	}

	public GaijiHttpException(final ErrorType error, final String message) {
		super(String.format("'%s' error occured. More information: '%s'", error.toString(), message));
	}
}
