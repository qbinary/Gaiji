package dev.qbin.gaiji.exception;

public class GaijiConfigException extends GaijiException {

	public GaijiConfigException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 210018346352835838L;

}
