package dev.qbin.gaiji;

import java.nio.file.Path;
import java.util.concurrent.ConcurrentLinkedQueue;

import dev.qbin.gaiji.exception.GaijiApiTokenNotSetException;
import dev.qbin.gaiji.util.PowerFiles;

/**
 * Singleton service class which handles some background meta stuff.
 * 
 * @author qbin
 * @version 1.0
 */
public class GaijiService {

	private static GaijiService instance = new GaijiService();
	private ConcurrentLinkedQueue<Path> tempDirectoryQueue = new ConcurrentLinkedQueue<>();

	private String apiToken = null;

	/**
	 * Registers custom shutdown hook which deletes all temp directories on runtime
	 * shutdown.
	 */
	private GaijiService() {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				tempDirectoryQueue.forEach(p -> {
					PowerFiles.deleteRecursive(p);
				});
			}
		}));
	}

	/**
	 * @return singleton instance of {@link GaijiService}
	 */
	public static final GaijiService getInstance() {
		return instance;
	}

	/**
	 * Adds the passed {@link Path} to the list of directories which will be deleted
	 * recursively by the shutdown hook.
	 * 
	 * @param toRegister {@link Path} to be deleted.
	 */
	public void registerTempDirectory(final Path toRegister) {
		tempDirectoryQueue.add(toRegister);
	}

	public void setApiToken(final String apiToken) {
		this.apiToken = apiToken;
	}

	public String getApiToken() throws GaijiApiTokenNotSetException {
		if(apiToken == null)
			throw new GaijiApiTokenNotSetException();
		return apiToken;
	}

}
