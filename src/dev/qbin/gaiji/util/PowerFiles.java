package dev.qbin.gaiji.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import dev.qbin.gaiji.Const;
import dev.qbin.gaiji.GaijiService;

/**
 * Utility for more advanced file operations.
 * 
 * @author qbin
 * @version 1.0
 *
 */
public class PowerFiles {
	private PowerFiles() {
	}

	/**
	 * Deletes given file and all files underneath
	 * 
	 * @param file {@link Path} file to delete
	 */
	public static void deleteRecursive(final Path file) {
		if (Files.isDirectory(file))
			try {
				Files.list(file).forEach(p -> deleteRecursive(p));
			} catch (IOException e) {
				Log.x(e);
			}

		try {
			Files.delete(file);
		} catch (IOException e) {
			Log.x(e);
		}
	}

	/**
	 * Creates a temp directory and registers it to the {@link GaijiService}
	 * 
	 * @return {@link Path} to the directory
	 * @throws IOException
	 */
	public static Path getManagedTempDirectory() throws IOException {
		Path tempDir = Files.createTempDirectory(Const.PROJECT_SIMPLE_NAME);
		GaijiService.getInstance().registerTempDirectory(tempDir);

		return tempDir;
	}

	/**
	 * Creates a log file and its parents directories.
	 * 
	 * @param parentDirectory the directory where the log file should be created
	 * @param prefix          for the log file
	 * @return the {@link Path} to the created file
	 * @throws IOException
	 */
	public static Path createLogFile(final Path parentDirectory, final String prefix) throws IOException {
		// Build log file task
		String pathTemplate = "%s/%s_%s.log";
		Path logFile = Paths.get(String.format(pathTemplate, parentDirectory.toAbsolutePath(), prefix,
				LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)));

		Files.createDirectories(logFile);
		Files.createFile(logFile);

		return logFile;
	}
}
