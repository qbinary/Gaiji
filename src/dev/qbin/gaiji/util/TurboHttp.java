package dev.qbin.gaiji.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class TurboHttp {
	@SafeVarargs
	public static String post(final String apiUrl, final String body, final Pair<String, String>... header)
			throws MalformedURLException, IOException {
		HttpURLConnection httpConnection = (HttpURLConnection) new URL(apiUrl).openConnection();

		// Request
		httpConnection.setRequestMethod("POST");
		httpConnection.setDoOutput(true);
		for (int i = 0; i < header.length; i++) {
			httpConnection.addRequestProperty(header[i].getY(), header[i].getZ());
		}
		httpConnection.getOutputStream().write(body.getBytes());

		// Retrieve and return response
		while (httpConnection.getInputStream().available() == 0) {
		}
		;

		byte[] buf = new byte[256];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while (httpConnection.getInputStream().read(buf) != -1)
			baos.writeBytes(buf);

		System.out.println(baos.toString());
		return baos.toString();
	}
	
}
