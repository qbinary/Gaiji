package dev.qbin.gaiji.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import dev.qbin.gaiji.Const;
import dev.qbin.gaiji.exception.GaijiConfigException;

public class GaijiConfig {

	private Properties configPrp;

	public GaijiConfig(final Path configLocation) throws IOException, GaijiConfigException {
		configPrp = new Properties();
		configPrp.load(Files.newInputStream(configLocation));
		for (final String key : Const.Config.REQUIRED_KEYS) {
			if (!configPrp.containsKey(key))
				throw new GaijiConfigException(String.format(Const.Text.CONFIG_KEY_MISSING, key));
		}
	}

	public String getApiToken() {
		return configPrp.getProperty(Const.Config.API_TOKEN);
	}
}
