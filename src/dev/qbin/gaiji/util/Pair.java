package dev.qbin.gaiji.util;

import java.util.Map.Entry;

/**
 * Simple key value pair class like an {@link Entry}.
 * 
 * @author qbin
 *
 * @param <Y> type of the first value
 * @param <Z> type of the second value
 */
public class Pair<Y, Z> {

	// Values
	private Y y;
	private Z z;

	/**
	 * Initialize pair with values x and y.
	 * 
	 * @param x first value
	 * @param y second value
	 */
	public Pair(final Y y, final Z z) {
		this.y = y;
		this.z = z;
	}

	/**
	 * Initialize pair with both values being null.
	 */
	public Pair() {
		this.y = null;
		this.z = null;
	}

	/**
	 * @return the first value
	 */
	public Y getY() {
		return y;
	}

	/**
	 * @return the second value
	 */
	public Z getZ() {
		return z;
	}

	/**
	 * Updates the first value
	 * 
	 * @param x value to update with
	 */
	public void setY(Y y) {
		this.y = y;
	}

	/**
	 * Updates the second value
	 * 
	 * @param y value to update with
	 */
	public void setZ(Z z) {
		this.z = z;
	}

	/**
	 * Generates a human readable string similar to an HTTP Header declaration.
	 * Relies on the to String method of the types X and Y.
	 */
	@Override
	public String toString() {
		return String.format("%s: %s", y.toString(), z.toString());
	}

	/**
	 * Quick and simple static way of initializing a pair.
	 * 
	 * @param <Y> first type
	 * @param <Z> seconds type
	 * @param y   first value
	 * @param z   second value
	 * @return the respecting {@link Pair<X, Y>} object
	 */
	public static <Y, Z> Pair<Y, Z> of(final Y y, final Z z) {
		return new Pair<Y, Z>(y, z);
	}
}
