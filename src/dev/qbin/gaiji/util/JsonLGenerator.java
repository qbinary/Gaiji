package dev.qbin.gaiji.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;

public class JsonLGenerator {
	private LinkedList<String> jsonlData = new LinkedList<>();
	private boolean cacheUpToDate = false;
	private String cache;

	public void addLine(final String line) {
		jsonlData.add(line);
		cacheUpToDate = false;
	}

	public void generateFile(final Path file, final String charset) throws IOException {
		Files.write(file, getCache().getBytes(charset), StandardOpenOption.CREATE);
	}

	public String getCache() {
		if (!cacheUpToDate)
			compileCache();
		return cache;
	}

	@Override
	public String toString() {
		return getCache();
	}

	private void compileCache() {
		StringBuilder cacheBuilder = new StringBuilder();
		jsonlData.forEach(s -> {
			cacheBuilder.append(s);
			cacheBuilder.append("\n");
		});
		cache = cacheBuilder.toString();
		cacheUpToDate = true;
	}
}
