package dev.qbin.gaiji.util;

public enum HttpMethod {
	POST("POST");

	private String id;

	private HttpMethod(final String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return id;
	}
}
