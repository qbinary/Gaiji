package dev.qbin.gaiji.util;

public enum HttpHeader {
	AUTHORIZATION("Authorization"), CONTENT_TYPE("Content-Type");

	private String httpKey;

	private HttpHeader(final String httpKey) {
		this.httpKey = httpKey;
	}

	@Override
	public String toString() {
		return httpKey;
	}
}
