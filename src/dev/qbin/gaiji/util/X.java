package dev.qbin.gaiji.util;

public class X {
	private X() {
	}

	public static final String asOneLine(final Exception e) {
		return String.format("Exception: '%s'; Msg: '%s'", e.getClass().getSimpleName(), e.getMessage());
	}
}
