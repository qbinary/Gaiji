package dev.qbin.gaiji.util;

import java.io.PrintStream;

public class Log {

	private static boolean verbose = false;

	public static void l(final Object content) {
		defaultPrintln(content);
	}

	public static void e(final Object content) {
		errorPrintln(content);
	}

	public static void x(final Exception content) {
		errorPrintln(String.format("Exception occured:\nxcptn#%s\nmsg#%s\nmsgLclzd#%s", content.getClass().getName(),
				content.getMessage(), content.getLocalizedMessage()));
	}

	private static void defaultPrintln(final Object content) {
		printlnStream(System.out, content);
	}

	private static void errorPrintln(final Object content) {
		printlnStream(System.err, content);
	}

	private static void printlnStream(final PrintStream stream, final Object content) {
		if (verbose)
			stream.println(content);
	}
	
	public static void setVerbose(boolean verbose) {
		Log.verbose = verbose;
	}
}
