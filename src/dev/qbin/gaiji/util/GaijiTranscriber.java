package dev.qbin.gaiji.util;

import dev.qbin.gaiji.openai.tune.GaijiTune;

/**
 * Class used to transcribe the communication with the AI.
 * 
 * @author qbin
 * @version 1.0
 */
public class GaijiTranscriber {
	private GaijiTune transscript = new GaijiTune();

	/**
	 * @param transcript {@link GaijiTranscriber} will apend to the passed
	 *                   {@link GaijiTune}
	 */
	public GaijiTranscriber(final GaijiTune transcript) {
		this.transscript = transcript;
	}

	/**
	 * Start with empty transcript.
	 */
	public GaijiTranscriber() {
	}

	/**
	 * Transcribes a conversation snippet
	 * @param prompt the prompt given to the AI
	 * @param response the response of the AI
	 */
	public synchronized void transscribe(String prompt, String response) {
		transscript.offer(new Pair<String, String>(prompt, response));
	}

	/**
	 * @return the transcript as {@link GaijiTune}
	 */
	public GaijiTune getTransscript() {
		return transscript;
	}
}
