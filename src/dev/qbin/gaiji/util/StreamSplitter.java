package dev.qbin.gaiji.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * Convenience class which allows to split one {@link InputStream} into multiple
 * {@link OutputStream}s using {@link BufferedReader} and {@link PrintWriter}s.
 * Operates line by line.
 * 
 * @author qbin
 * @version 2.0
 */
public class StreamSplitter implements Runnable {

	private BufferedReader source;
	private List<PrintStream> printStreams;
	private Living living;

	/**
	 * 
	 * @param living  object implementing {@link Living}. Used to determine if the
	 *                thread should still run
	 * @param source  the stream which will be mirrored to the output streams
	 * @param streams the output streams
	 */
	public StreamSplitter(final Living living, final InputStream source, final OutputStream... streams) {
		this.source = new BufferedReader(new InputStreamReader(source));
		this.living = living;

		// Initialize print streams
		this.printStreams = new LinkedList<>();
		for (final OutputStream stream : streams) {
			this.printStreams.add(new PrintStream(stream));
		}
	}

	/**
	 * Creates and starts a StreamSplitter Thread.
	 * 
	 * @param living  see constructor
	 * @param source  see constructor
	 * @param streams see constructor
	 */
	public static final Thread dispatch(final Living living, final InputStream source, final OutputStream... streams) {
		Thread thread = new Thread(new StreamSplitter(living, source, streams));
		thread.start();
		return thread;
	}

	@Override
	public void run() {
		try {
			do {

				String line = null;
				while ((line = source.readLine()) != null) {
					for (final PrintStream stream : printStreams)
						stream.println(line);
				}

			} while (living.isAlive());
		} catch (FileNotFoundException e) {
			Log.x(e);
		} catch (IOException e) {
			Log.x(e);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		// Close all print streams
		for (final PrintStream stream : this.printStreams) {
			stream.close();
		}
	}
}
