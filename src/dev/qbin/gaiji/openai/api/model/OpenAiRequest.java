package dev.qbin.gaiji.openai.api.model;

public class OpenAiRequest {

	public static class CreateFineTuneRequestBuilder {

		public class CreateFineTuneRequest {
			private CreateFineTuneRequest() {
			}

			public String training_file;
			public String model;
		}

		private CreateFineTuneRequest request = new CreateFineTuneRequest();

		public CreateFineTuneRequestBuilder setTrainFile(String trainingFile) {
			this.request.training_file = trainingFile;
			return this;
		}

		public CreateFineTuneRequestBuilder setModel(String model) {
			this.request.model = model;
			return this;
		}

		public CreateFineTuneRequest build() {
			return request;
		}
	}

	public static class CompletionRequestBuilder {

		public class CompletionRequest {
			private CompletionRequest() {
			}

			public String prompt = "";
			public String suffix = null;
			public int max_tokens = 16;
			public double temperature = 1;
			public double top_p = 1;
			public int n = 1;
			// public boolean stream = false; // >> Not supported by Gaiji
			public Integer logprobs = null;
			public boolean echo = false;
			public String stop = null;
			public double presence_penalty = 0;
			public double frequency_penalty = 0;
			public int best_of = 1;
			// public Map<?,?> logit_bias = null; // >> Not supported by Gaiji yet
			public String user = "gaiji";

			public String model = null;
		}

		private CompletionRequest request;

		public CompletionRequestBuilder() {
			this.request = new CompletionRequest();
		}

		public CompletionRequestBuilder setPrompt(String prompt) {
			this.request.prompt = prompt;
			return this;
		}

		public CompletionRequestBuilder setSuffix(String suffix) {
			this.request.suffix = suffix;
			return this;
		}

		public CompletionRequestBuilder setMaxTokens(int maxTokens) {
			this.request.max_tokens = maxTokens;
			return this;
		}

		public CompletionRequestBuilder setTemperature(double temperature) {
			this.request.temperature = temperature;
			return this;
		}

		public CompletionRequestBuilder setTopP(double topP) {
			this.request.top_p = topP;
			return this;
		}

		public CompletionRequestBuilder setN(int n) {
			this.request.n = n;
			return this;
		}

		public CompletionRequestBuilder setLogprobs(Integer logprobs) {
			this.request.logprobs = logprobs;
			return this;
		}

		public CompletionRequestBuilder setEcho(boolean echo) {
			this.request.echo = echo;
			return this;
		}

		public CompletionRequestBuilder setStop(String stop) {
			this.request.stop = stop;
			return this;
		}

		public CompletionRequestBuilder setPresence_penalty(double presencePenalty) {
			this.request.presence_penalty = presencePenalty;
			return this;
		}

		public CompletionRequestBuilder setFrequency_penalty(double frequencyPenalty) {
			this.request.frequency_penalty = frequencyPenalty;
			return this;
		}

		public CompletionRequestBuilder setBest_of(int bestOf) {
			this.request.best_of = bestOf;
			return this;
		}

		public CompletionRequestBuilder setUser(String user) {
			this.request.user = user;
			return this;
		}

		public CompletionRequestBuilder setModel(String model) {
			this.request.model = model;
			return this;
		}

		public CompletionRequest build() {
			return request;
		}
	}
}
