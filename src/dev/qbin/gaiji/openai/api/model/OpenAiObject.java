package dev.qbin.gaiji.openai.api.model;

public class OpenAiObject {
	public String object;
	public String id;

	//// Lists
	public class OpenAiList<T> extends OpenAiObject {
		private OpenAiList() {
		}

		public T[] data;
	}

	public class FileList extends OpenAiList<File> {
		private FileList() {
		}
	}

	public class FineTuneList extends OpenAiList<FineTune> {
		private FineTuneList() {
		}
	}

	public class EngineList extends OpenAiList<Engine> {
		private EngineList() {
		}
	}

	// File
	public class File extends OpenAiObject {
		private File() {
		}

		public String purpose;
		public String filename;
		public long bytes;
		public long created_at;
		public String status;
		public String status_details;
	}

	// FineTune
	public class FineTune extends OpenAiObject {
		private FineTune() {
		}

		public class HyperParams {
			private HyperParams() {
			}

			public int n_epochs;
			public int batch_size;
			public double prompt_loss_weight;
			public double learning_rate_multiplier;
		}

		public HyperParams hyperparams;
		public String organization_id;
		public String model;
		public File[] training_files;
		public File[] validation_files;
		public File[] result_files;
		public long created_at;
		public long updated_at;
		public String status;
		public String fine_tuned_model;
	}

	// Engine
	public class Engine extends OpenAiObject {
		private Engine() {
		}

		public String owner;
		public boolean ready;
	}

	// Completion
	public class Completion {
		private Completion() {
		}

		public class Choice {
			private Choice() {
			}

			public String text;
			public int index;
			public String logprobs;
			public String finish_reason;
		}

		public String id;
		public String object;
		public String created;
		public String model;
		public Choice[] choices;
	}
}
