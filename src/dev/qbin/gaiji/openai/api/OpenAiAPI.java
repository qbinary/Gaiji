package dev.qbin.gaiji.openai.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import dev.qbin.gaiji.Const;
import dev.qbin.gaiji.exception.GaijiHttpException;
import dev.qbin.gaiji.exception.GaijiHttpException.ErrorType;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.Completion;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.EngineList;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.File;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.FineTune;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.FineTuneList;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CompletionRequestBuilder.CompletionRequest;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CreateFineTuneRequestBuilder;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CreateFineTuneRequestBuilder.CreateFineTuneRequest;
import dev.qbin.gaiji.util.Log;
import dev.qbin.gaiji.util.X;

public class OpenAiAPI {
	private Gson json;
	private String apiToken;

	public OpenAiAPI(final String apiToken) {
		this.apiToken = apiToken;
		this.json = new Gson();
	}

	public EngineList getEngines() throws JsonSyntaxException, JsonIOException, MalformedURLException, IOException {
		return json.fromJson(new InputStreamReader(get(Const.API.ENGINES).getInputStream()), EngineList.class);
	}

	public FineTuneList getFineTunes() throws JsonSyntaxException, JsonIOException, MalformedURLException, IOException {
		return json.fromJson(new InputStreamReader(get(Const.API.FINE_TUNES).getInputStream()), FineTuneList.class);
	}

	public FineTune getFineTune(final String id) throws MalformedURLException, IOException {
		HttpsURLConnection https = get(String.format(Const.API.FINE_TUNES_TEMPLATE, id));
		return json.fromJson(new InputStreamReader(https.getInputStream()), FineTune.class);
	}

	public FineTune getFineTune(final FineTune baseTune) throws MalformedURLException, IOException {
		return this.getFineTune(baseTune.id);
	}

	public void deleteModel(final String id) throws MalformedURLException, IOException {
		deleteFineTune(String.format(Const.API.DELETE_MODEL_TEMPLATE, id));
	}

	public void deleteModel(final FineTune fineTune) throws MalformedURLException, IOException {
		this.deleteModel(fineTune.model);
	}

	public void cancelFineTune(final String id) throws MalformedURLException, IOException {
		post(String.format(Const.API.CANCEL_FINE_TUNE, id));
	}

	public void cancelFineTune(final FineTune fineTune) throws MalformedURLException, IOException {
		this.cancelFineTune(fineTune.fine_tuned_model);
	}

	private void deleteFineTune(final String spec) throws MalformedURLException, IOException {
		HttpsURLConnection http = buildConnection(spec);
		http.setRequestMethod(Const.Http.DELETE);
	}

	public File uploadFile(final byte[] data, final String fileName) throws IOException {
		// TODO Rewrite

		// Create client
		CloseableHttpClient httpClient = HttpClients.createDefault();

		// Create post method
		HttpPost uploadFile = new HttpPost(Const.API.FILES);

		uploadFile.setHeader(Const.Http.Header.AUTHORIZATION,
				String.format(Const.API.AUTHORIZATION_TEMPLATE, apiToken));

		// Create multiparty builder
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();

		// Add purpose body
		builder.addTextBody("purpose", "fine-tune");

		// Add file
		builder.addBinaryBody("file", new ByteArrayInputStream(data), ContentType.APPLICATION_OCTET_STREAM, fileName);

		// Build multipart
		HttpEntity multipart = builder.build();

		// set multipart as entity of the post request
		uploadFile.setEntity(multipart);

		// Execute post and retrieve response
		CloseableHttpResponse response = httpClient.execute(uploadFile);

		// Extract response entity
		HttpEntity responseEntity = response.getEntity();
		String responseString = new String(responseEntity.getContent().readAllBytes());
		Log.l(responseString);

		return json.fromJson(responseString, File.class);
	}

	public FineTune createFineTune(final File trainFile, final String prefix, final String baseModel)
			throws IOException {
		HttpsURLConnection https = post(Const.API.FINE_TUNES);
		https.addRequestProperty(Const.Http.Header.CONTENT_TYPE, Const.Mime.JSON);

		// Build request object
		CreateFineTuneRequestBuilder request = new CreateFineTuneRequestBuilder();
		request.setTrainFile(trainFile.id);
		request.setModel(baseModel);

		// Set body
		https.setDoOutput(true);
		https.getOutputStream().write(json.toJson(request.build(), CreateFineTuneRequest.class).getBytes());

		try {
			return json.fromJson(new InputStreamReader(https.getInputStream()), FineTune.class);
		} catch (JsonSyntaxException e) {
			throw e;
		} catch (JsonIOException e) {
			throw e;
		} catch (IOException e) {
			try {
				System.err.println(new String(https.getErrorStream().readAllBytes()));
			} catch (IOException e1) {
			}
			throw e;
		}
	}

	public Completion createCompletion(final CompletionRequest completionRequest) throws GaijiHttpException {
		// Serialize body
		String bodyString = json.toJson(completionRequest);
		Log.l(bodyString);

		byte[] bodyBytes = bodyString.getBytes();

		// Open connection
		HttpsURLConnection openaiApi;
		try {
			openaiApi = post(Const.API.TUNE_COMPLETIONS);
		} catch (IOException e) {
			throw new GaijiHttpException(ErrorType.OPEN_URL_CONNECTION, X.asOneLine(e));
		}

		// Configure connection
		openaiApi.setDoInput(true);
		openaiApi.setDoOutput(true);

		// Content type header
		openaiApi.addRequestProperty(Const.Http.Header.CONTENT_TYPE, Const.Mime.JSON);

		// Write body
		try {
			openaiApi.getOutputStream().write(bodyBytes);
		} catch (IOException e) {
			throw new GaijiHttpException(ErrorType.WRITE_BODY, X.asOneLine(e));
		}

		// Retrieve response
		try {
			InputStream responseStream = openaiApi.getInputStream();

			// Await response
			while (responseStream.available() == 0) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
				}
			}
			// Retrieve response
			String responseString = new String(responseStream.readAllBytes());
			Log.l(responseString);
			return json.fromJson(responseString, Completion.class);

		} catch (IOException e) {
			try {
				System.err.println(new String(openaiApi.getErrorStream().readAllBytes()));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			throw new GaijiHttpException(ErrorType.RESPONSE_RETRIEVAL, X.asOneLine(e));
		}
	}

	private HttpsURLConnection get(final String spec) throws MalformedURLException, IOException {
		HttpsURLConnection https = buildConnection(spec);
		https.setRequestMethod(Const.Http.GET);
		return https;
	}

	private HttpsURLConnection post(final String spec) throws MalformedURLException, IOException {
		HttpsURLConnection http = buildConnection(spec);
		http.setRequestMethod(Const.Http.POST);
		return http;
	}

	private HttpsURLConnection buildConnection(final String spec) throws MalformedURLException, IOException {
		HttpsURLConnection http = (HttpsURLConnection) new URL(spec).openConnection();
		http.addRequestProperty(Const.Http.Header.AUTHORIZATION,
				String.format(Const.API.AUTHORIZATION_TEMPLATE, apiToken));
		return http;
	}

}
