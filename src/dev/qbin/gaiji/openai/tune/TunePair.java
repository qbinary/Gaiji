package dev.qbin.gaiji.openai.tune;

public class TunePair {
	public final String prompt;
	public final String completion;

	public TunePair(final String prompt, final String completion) {
		this.prompt = prompt;
		this.completion = completion;
	}
}
