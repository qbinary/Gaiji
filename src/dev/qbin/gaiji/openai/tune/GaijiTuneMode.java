package dev.qbin.gaiji.openai.tune;

public enum GaijiTuneMode {
	DISCARD, TUNE, CORRECT
}
