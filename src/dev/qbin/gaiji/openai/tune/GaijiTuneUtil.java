package dev.qbin.gaiji.openai.tune;

import dev.qbin.gaiji.util.Pair;

/**
 * Utility to filter a transcript in a weighted traning/tuning model.
 * 
 * @author qbin
 * @version 1.0
 */
public class GaijiTuneUtil {
	private GaijiTune raw = new GaijiTune();
	private GaijiTune rated = new GaijiTune();

	private Pair<String, String> currentSubject = null;

	public GaijiTuneUtil(final GaijiTune transScript) {
		this.raw = transScript;
	}

	/**
	 * Fetches the next subject if the last was handled and return true. If the last
	 * was not handled nothing happens and the method returns false.
	 */
	public synchronized boolean nextSubject() {
		if (hasSubject())
			return false;
		currentSubject = raw.poll();
		return true;
	}

	/**
	 * @return the current subject
	 */
	public Pair<String, String> peekSubject() {
		return currentSubject;
	}

	/**
	 * Handles the subject. Add it to the training model.
	 */
	public void tuneSubject() {
		rated.offer(currentSubject);
		dismissSubject();
	}

	/**
	 * Handles the subject. Dismiss the subject by not adding it to the training
	 * model.
	 */
	public void dismissSubject() {
		currentSubject = null;
	}

	/**
	 * Handles the subject. Edits the subject respectively to the parameter and adds
	 * its edited form to the training model.
	 * 
	 * @param correction replaces the answer of the current subject
	 */
	public void correctSubject(final String correction) {
		currentSubject.setY(correction);
		tuneSubject();
	}

	/**
	 * @return if there is a subject currently.
	 */
	public boolean hasSubject() {
		return currentSubject != null;
	}

	/**
	 * @return the training model at its current state and creates a new internal
	 *         one
	 */
	public GaijiTune pollRated() {
		GaijiTune rated = this.rated;
		rated = null;
		return rated;
	}
}
