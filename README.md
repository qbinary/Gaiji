# Gaiji -- the Java OpenAI interface

## Name
The name was generated by GPT-3.

## Getting started
### Simple completion example
```java
public static main(String[] args) {
	// Instantiate a GPT3 adapter object
	GPT3 gpt = new GPT3("API-TOKEN");
	
	// Initialize API
	OpenAiAPI api = new OpenAiAPI(config.getApiToken());
	
	// Build request
	CompletionRequest request = new OpenAiRequest.CompletionRequestBuilder().
		setPrompt("Hello how are you?\n").
		setTemperature(0.7D).
		setModel("text-davinci-002").build();
	
	// Execute request
	Completion completion = api.createCompletion(request);
	
	// Print out result
	System.out.println(completion.choices[0].text);    
}
```

## Features
### `OpenAiAPI` - OpenAi API adapter
* List all engines (`getEngines()`)
* Access the fine-tune (jobs) (`getFineTunes` / `getFineTune`)
* Create, cancel and delete fine-tunes/models (`createFineTune` / `cancelFineTune` / `deleteFineTune`)
* Upload file to the file api (`uploadFile`)

### `OpenAiObject` - OpenAi json object wrapper collection
Currently supported:
* File
    * List of files
* Finetune
    * List of finetunes
* Engine
    * List of engines